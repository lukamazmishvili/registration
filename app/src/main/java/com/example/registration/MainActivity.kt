package com.example.registration

import android.media.MediaCodec
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private lateinit var EmailBox : EditText
    private lateinit var PassBox : EditText
    private lateinit var ConfirmPassBox : EditText
    private lateinit var SubmitButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        submitButton()

    }

    private fun init() {

        EmailBox = findViewById(R.id.EmailBox)
        PassBox = findViewById(R.id.PassBox)
        ConfirmPassBox = findViewById(R.id.ConfirmPassBox)
        SubmitButton = findViewById(R.id.SubmitButton)

    }

    private fun submitButton() {

        SubmitButton.setOnClickListener {

            val email = EmailBox.text.toString().trim()
            val pass = PassBox.text.toString().trim()
            val confirmPass = ConfirmPassBox.text.toString().trim()

            if ("@" !in email){

                EmailBox.error = "Incorrect!"
                return@setOnClickListener

            }else if ("." !in email){

                EmailBox.error = "Incorrect!"
                return@setOnClickListener

            }else if (email.length < 5) {

                EmailBox.error = "Incorrect!"
                return@setOnClickListener

            }else if (pass.length < 9) {

                PassBox.error = "Must be 9 or more characters"
                return@setOnClickListener

            }else if (pass.isDigitsOnly()) {

                PassBox.error = "Must contain letters"
                return@setOnClickListener

            }else if (!pass.matches(".*[0-9].*".toRegex())){

                PassBox.error = "Must contain numbers"
                return@setOnClickListener

            }else if (confirmPass != pass){

                ConfirmPassBox.error = "Password doesn't match"
                return@setOnClickListener

            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, pass)

                .addOnCompleteListener { task ->

                    if (task.isSuccessful){

                        Toast.makeText(this, "Successfully Registered!", Toast.LENGTH_SHORT).show()

                    }else {
                        Toast.makeText(this, "Already Registered!", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
}